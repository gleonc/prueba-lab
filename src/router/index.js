import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Crud from "../views/crud.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/Crud",
    name: "Crud",
    component: Crud,
  },
  
];

const router = new VueRouter({
  routes,
});

export default router;
